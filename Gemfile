# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |_repo| 'https://github.com/#{repo}.git' }

ruby '3.1.0'

gem 'pg', '~> 1.1'
gem 'rails', '~> 7.0.4'

gem 'bootsnap', require: false
gem 'puma', '~> 5.0'
gem 'rack-cors'
gem 'rails-i18n', '~> 7.0', '>= 7.0.6'

gem 'redis', '~> 4.0'
gem 'sidekiq', '~> 7.0', '>= 7.0.9'

gem 'paranoia', '~> 2.6', '>= 2.6.1'
gem 'enumerize', '~> 2.5'

gem 'activerecord-import', '~> 1.4', '>= 1.4.1'
gem 'bcrypt', '~> 3.1.7'
gem 'jsonapi-authorization', '~> 3.0', '>= 3.0.2'
gem 'jsonapi-resources'
gem 'jwt', '~> 2.5'
gem 'pundit', '~> 2.2'

gem 'faker', '~> 3.0'
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# App configuration environment
gem 'figaro', '~> 1.2'

group :development, :test do
  gem 'byebug', '~> 11.1', '>= 11.1.3'
  gem 'dotenv-rails'
  gem 'rspec-rails', '~> 6.0', '>= 6.0.1'
end

group :development do
  gem 'brakeman', '~> 5.3', '>= 5.3.1'
  gem 'rubocop-rails', require: false
  gem 'spring'
end

group :test do
  gem 'database_cleaner', '~> 2.0', '>= 2.0.1'
  gem 'factory_bot_rails', '~> 6.2'
  gem 'shoulda-matchers', '~> 5.2'
end
