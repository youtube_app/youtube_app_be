module Searchable
  extend ActiveSupport::Concern

  class_methods do
    def search_text(field_names = [:name, :code])
      filter :search, apply: ->(records, value, _options) {
        query_sql = field_names.map do |field_name|
          generate_sql_search_query("#{records.table.name}.#{field_name}", "%#{value[0].downcase}%") if value[0]
        end.compact.join(' OR ')

        records.where(query_sql)
      }
    end

    def generate_sql_search_query(field_name, value)
      ActiveRecord::Base.send(:sanitize_sql_array, ["lower(#{field_name}) ILIKE ?", value])
    end
  end
end
