module V1
  class UserResource < ApplicationResource
    attributes(
      :username,
      :full_name,
      :email,
      :is_active,
      :img,
      :password,
      :password_confirmation,
      :subscribers,
      :subscribed_users,
      :created_at,
    )

    has_many :videos

    after_create :send_email_notification

    search_text([:full_name, :email, :username])

    def self.fields
      super - [:password, :password_confirmation]
    end


    def self.updatable_fields(context)
      super - [ :username]
    end

    def send_email_notification
    end
  end
end
