module V1
  class ApplicationResource < JSONAPI::Resource
    include JSONAPI::Authorization::PunditScopedResource
    include ::Searchable

    abstract

    def add_audit_info
      if @model.new_record?
        @model.created_by_id = context[:current_user].id
      end
    end

    def self.default_sort
      [{ field: 'created_at', direction: :desc }]
    end
  end
end
