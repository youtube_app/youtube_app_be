module V1
  class VideoResource < ApplicationResource
    attributes(
      :title,
      :desc,
      :img_url,
      :video_url,
      :created_by_id,
      :views,
      :likes,
      :dislikes,
      :tags,
      :created_at
    )

    has_one :created_by
    before_save :add_audit_info
    after_create :send_notification
    search_text([:title])

    filter :tags, apply: ->(records, value, _options) {
      query_string = ['?=any(tags)'] * value.length
      records.where(query_string.join(' or '), *value)
    }

    filter :type, apply: ->(records, value, options) {
      current_user = options[:context][:current_user]
      case value[0]
        when 'trend'
          records.trends
        when 'sub'
          records.where(created_by_id: current_user.subscribed_users)
        when 'lib'
          records.where(created_by_id: current_user.id)
        else
          records.randoms
      end
    }

    def send_notification
      message = {
        content: "#{@context[:current_user][:full_name]} uploaded: #{@model.title}",
        created_by_id: @context[:current_user].id,
        video_id: @model.id
      }
      ActionCable.server.broadcast 'messages_channel', message
    end
  end
end
