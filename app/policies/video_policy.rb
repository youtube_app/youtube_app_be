class VideoPolicy < ApplicationPolicy

  def show?
    true
  end

  def index?
    true
  end

  def create?
    true
  end

  def update?
    true
  end

  def lib?
    true
  end
end
