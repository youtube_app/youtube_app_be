class UserPolicy < ApplicationPolicy

  def show?
    true
  end

  def index?
    true
  end

  def create?
    true
  end

  def update?
    true
  end

  def reset_password?
    true
  end

  def deactive?
    true
  end

  def active?
    true
  end

  def subscribe?
    true
  end

  def unsubscribe?
    true
  end

  def like?
    true
  end

  def dislike?
    true
  end
end
