module V1
  class AuthController < ApplicationController
    skip_before_action :authenticate, only: :signin
    before_action :use_application, only: :signin

    def signin
      token = @application.login(permitted_signin_params[:username], permitted_signin_params[:password])

      render_jwt_token(token)
    end

    def me
      jsonapi_render(
        json: @current_user,
        resource_class: UserResource
      )
    end

    private

    def use_application
      @application = AuthApplication.new
    end

    def permitted_signin_params
      params.require(:data)
        .require(:attributes)
        .permit(
          :username, :password
        )
    end
  end
end
