module V1
  class VideosController < ApplicationController
    skip_before_action :authenticate, only: [:show]
    before_action :use_application

    def like
      authorize @current_user, :like?
      result = @application.like(params[:id])
      render json: {success: result}
    end

    def dislike
      authorize @current_user, :like?
      result = @application.dislike(params[:id])
      render json: {success: result}
    end

    private

    def use_application
      @application = VideoApplication.new(@current_user)
    end
  end
end
