module V1
  class UsersController < ApplicationController
    skip_before_action :authenticate, only: [:create, :index, :show]
    before_action :set_channel_user,
    only: [
      :subscribe, :unsubscribe
    ]
    before_action :use_application

    def subscribe
      authorize @current_user, :subscribe?
      result = @application.subscribe(params[:id])

      render json: {success: result}
    end

    def unsubscribe
      authorize @current_user, :unsubscribe?

      @application.subscribe(params[:id])
      render json: {success: result}
    end

    private

    def use_application
      @application = UserApplication.new(@current_user, @channel_user)
    end

    def set_channel_user
      @channel_user = User.find(params[:id])
    end

    def change_password_params
      params.required(:data).required(:attributes).permit(
        :currentPassword,
        :password,
        :passwordConfirmation
      )
    end
  end
end
