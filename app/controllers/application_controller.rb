# frozen_string_literal: true

class ApplicationController < ActionController::API
  include JSONAPI::ActsAsResourceController
  include Pundit::Authorization
  include JsonapiResponse
  include ExceptionHandler

  ACCESS_TOKEN = 'Access-Token'

  before_action :authenticate

  def context
    {
      current_user: @current_user,
      user: current_user
    }
  end

  def authenticate
    app = AuthApplication.new
    result = app.authenticate_request(request.headers[ACCESS_TOKEN])
    @current_user = result[:user]
  end

  def current_user
    {
      current_user: @current_user
    }
  end
end
