module JsonapiResponse
  def render_jwt_token(token, status = :ok)
    result = { token: }

    render json: result, status:
  end

  def render_jsonapi(data, status = :ok)
    result = { data: { attributes: data } }

    render json: result, status:
  end

  def render_jsonapi_error(errors, status = :unprocessable_entity)
    render json: { errors: [errors] }, status:
  end

  def jsonapi_render(json:, status: nil, resource_class:, options: {})
    body = json.nil? ? { data: nil } : jsonapi_format(json, options, resource_class)
    render json: body, status: (status || :ok)
  end

  def jsonapi_format(object, options, resource_class)
    opts = apply_params_parser

    serializer = JSONAPI::ResourceSerializer.new(resource_class, opts)
    resource = if object.respond_to?(:to_ary)
      object.map do |obj|
        resource_class.new(obj, context)
      end
    else
      resource_class.new(object, context)
    end

    result = serializer.serialize_to_hash(resource)
    result.merge!(meta: options[:top_level_meta]) if options[:top_level_meta].present?

    result
  end

  def apply_params_parser
    opts = {}
    # includes
    opts[:include] = params[:include].split(',').map(&:underscore) if params[:include].present?

    opts
  end
end
