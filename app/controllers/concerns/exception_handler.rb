module ExceptionHandler
  extend ActiveSupport::Concern

  # Define custom error subclasses - rescue catches `StandardErrors`
  class AuthenticationError < StandardError; end

  class Forbidden < StandardError; end

  class Unauthorized < StandardError; end

  class MissingToken < StandardError; end

  class InvalidToken < StandardError; end

  class ExpiredToken < StandardError; end

  class Validation < StandardError; end

  class BusinessRuleError < StandardError; end

  included do
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::Forbidden, with: :forbidden_request
    rescue_from ExceptionHandler::MissingToken, with: :unauthorized_request
    rescue_from ExceptionHandler::InvalidToken, with: :unauthorized_request
    rescue_from ExceptionHandler::ExpiredToken, with: :unauthorized_request
    rescue_from ExceptionHandler::Unauthorized, with: :unauthorized_request
    rescue_from ExceptionHandler::BusinessRuleError, with: :unprocessable_request
    rescue_from Pundit::NotAuthorizedError, with: :forbidden_pundit_request
    rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_request
  end

  private

  def forbidden_pundit_request
    render_jsonapi_error({ detail: 'unauthorized', status: :forbidden }, :forbidden)
  end

  def unauthorized_request(e)
    render_jsonapi_error({ detail: e.message, status: :unauthorized }, :unauthorized)
  end

  def forbidden_request(e)
    render_jsonapi_error({ detail: e.message, status: :forbidden }, :forbidden)
  end

  def unprocessable_request(e)
    render_jsonapi_error({ detail: e.message, status: :unprocessable_entity }, :unprocessable_entity)
  end

  def header_row_not_found(e)
    headers = JSON.parse(e.message).join(', ')
    message = 'invalid_template'
    render_jsonapi_error({ detail: message, status: :unprocessable_entity }, :unprocessable_entity)
  end
end
