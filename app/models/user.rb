class User < ApplicationRecord
  extend Enumerize
  include Constants

  before_save { email.downcase! }
  before_save { username.downcase! }

  has_secure_password

  validates :full_name, presence: true, length: { maximum: 255 }
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  validates :username, presence: true, length: { maximum: 100 },
            uniqueness: { case_sensitive: false }

  has_many :videos

  def self.find_by_username_or_email(username)
    username = username.downcase

    User.where(username:).or(User.where(email: username)).first
  end
end
