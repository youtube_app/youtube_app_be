class Video < ApplicationRecord
  validates :title, presence: true, length: { maximum: 255 }
  validates :video_url, presence: true

  belongs_to :created_by, class_name: 'User', foreign_key: :created_by_id, optional: true

  scope :randoms, -> {where(id: self.ids.sample(40))}
  scope :trends, -> {where('views >= 50')}
end
