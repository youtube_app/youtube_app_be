class UserApplication
  attr_reader :current_user

  def initialize(current_user, channel_user = nil)
    @current_user = current_user
    @channel_user = channel_user
  end

  def subscribe(id)
    @current_user.subscribed_users << id

    if @current_user.save
      @channel_user.subscribers += 1
      @channel_user.save
    end
    if video.save
      return "Subcription successful."
    else
      return "Something went wrong."
    end
  end

  def unsubscribe(id)
    @current_user.subscribed_users.delete(id)

    if @current_user.save
      @channel_user.subscribers -= 1
      @channel_user.save
    end

    if video.save
      return "UnSubcription successful."
    else
      return "Something went wrong."
    end

  end
end
