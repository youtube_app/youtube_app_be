class VideoApplication
  attr_reader :current_user

  def initialize(current_user)
    @current_user = current_user
  end

  def like(id)
    video = Video.find(id)
    video.likes << @current_user.id
    video.dislikes.delete(@current_user.id)

    if video.save
      return "The video has been liked."
    else
      return "Something went wrong."
    end
  end

  def dislike
    video = Video.find(id)
    video.dislikes << @current_user.id
    video.likes.delete(@current_user.id)

    if video.save
      return "The video has been disliked."
    else
      return "Something went wrong."
    end
  end
end
