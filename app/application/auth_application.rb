class AuthApplication
  attr_reader :jwt_service

  def initialize
    @jwt_service = JwtService.new
  end

  def login(username, password)
    user = current_user(username, password)

    jwt_service.encode(payload(user)) if user.present?
  end

  def authenticate_request(token)
    decoded_auth_token = jwt_service.decode(token)

    {
      user: User.find(decoded_auth_token[:data][:user_id]),
    }
  end

  private

  def payload(user)
    {
      data: {
        user_id: user.id,
      },
      iat: Time.current.to_i,
      exp: 100.hour.from_now.to_i
    }
  end

  def current_user(username, password)
    @current_user ||= begin
      user = User.find_by_username_or_email(username)
      if user&.authenticate(password)
        raise(
          ExceptionHandler::AuthenticationError,
          'user is blocked'
        ) unless user.is_active

        user.reload
      else
        raise(ExceptionHandler::AuthenticationError, 'invalid credentials')
      end
    end
  end
end
