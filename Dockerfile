FROM ruby:3.1.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /youtube_app_be
WORKDIR /youtube_app_be
ADD Gemfile /youtube_app_be/Gemfile
ADD Gemfile.lock /youtube_app_be/Gemfile.lock
RUN bundle install
ADD . /youtube_app_be

CMD ["puma"]