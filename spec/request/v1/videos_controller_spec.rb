require 'rails_helper'

RSpec.describe V1::VideosController, type: :request do
  let(:user) { create('user') }
  let(:video) { create('video') }
  let(:video1) { build('video') }
  let(:token) { JwtHelper.generate_token(user.username) }
  let(:headers) do
    {
      'Content-Type' => 'application/vnd.api+json',
      'Access-Token' => token
    }
  end

  let(:parameters) do
    {
      data: {
        type: 'videos',
        attributes: video
      }
  }
  end

  let(:update_parameters) do
    {
      data: {
        id: video.id,
        type: 'videos',
        attributes: {
          "title": video1.title,
          "desc": video1.desc,
          "imgUrl": video1.img_url,
          "videoUrl": video1.video_url
        }
      }
  }
  end

  describe 'GET /v1/videos' do
    subject(:do_request) do
      get "/v1/videos", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'POST /v1/videos' do
    subject(:do_request) do
      post '/v1/videos', headers: headers, :params => parameters.to_json
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PUT /v1/videos' do
    subject(:do_request) do
      put "/v1/videos/#{video.id}", headers: headers, :params => update_parameters.to_json
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET /v1/videos/sub' do
    subject(:do_request) do
      get "/v1/videos/sub", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET /v1/videos/random' do
    subject(:do_request) do
      get "/v1/videos/random", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(200)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET /v1/videos/lib' do
    subject(:do_request) do
      get "/v1/videos/lib", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET /v1/videos/:id' do
    subject(:do_request) do
      get "/v1/videos?filter[id]=#{video.id}", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(200)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end
end
