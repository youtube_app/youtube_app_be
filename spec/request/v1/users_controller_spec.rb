require 'rails_helper'

RSpec.describe V1::UsersController, type: :request do
  let(:user) { create('user') }
  let(:user1) { create('user') }
  let(:video) { create('video') }
  let(:token) { JwtHelper.generate_token(user.username) }
  let(:headers) do
    {
      'Content-Type' => 'application/vnd.api+json',
      'Access-Token' => token
    }
  end

  describe 'GET /v1/users' do
    subject(:do_request) do
      get "/v1/users", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET /v1/users/:id' do
    subject(:do_request) do
      get "/v1/users?filter[id]=#{user.id}", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PUT /v1/users/:id/subscribe' do
    subject(:do_request) do
      put "/v1/users/#{user1.id}/subscribe", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PUT /v1/users/:id/unsubscribe' do
    subject(:do_request) do
      put "/v1/users/#{user1.id}/unsubscribe", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PUT /v1/users/:id/like' do
    subject(:do_request) do
      put "/v1/users/#{video.id}/like", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PUT /v1/users/:id/dislike' do
    subject(:do_request) do
      put "/v1/users/#{video.id}/dislike", headers: headers
    end

    context 'when user is anonymous' do
      let(:token) { nil }

      it do
        do_request
        expect(response).to have_http_status(401)
      end
    end

    context 'when user is login success' do
      it do
        do_request
        expect(response).to have_http_status(:success)
      end
    end
  end

end
