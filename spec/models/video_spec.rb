require 'rails_helper'

RSpec.describe Video, type: :model do
   # Association test
   it { should belong_to(:created_by).optional }

   # Validation tests
   describe 'validation' do
     subject { FactoryBot.build(:video) }

     it { should validate_presence_of(:title) }
     it { should validate_presence_of(:video_url) }
   end
end
