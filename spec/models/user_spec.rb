require 'rails_helper'

RSpec.describe User, type: :model do
  # Validation tests
  describe 'validation' do
    subject { FactoryBot.build(:user) }

    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:full_name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end
end
