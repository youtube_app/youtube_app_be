require "openssl"
require "active_support/all"
require "jwt"

module JwtHelper
  def self.generate_token(username)
    include ActiveSupport
    user = User.find_by_username_or_email(username)
    payload = {
      data: {
        user_id: user.id,
      },
      iat: Time.current.to_i,
      exp: 100.hour.from_now.to_i
    }
    JWT.encode(payload, Rails.application.credentials.secret_key_base)
  end
end
