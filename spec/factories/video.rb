FactoryBot.define do
  factory :video, class: Video do
    title { Faker::Music::GratefulDead.song }
    desc {Faker::Quote.famous_last_words}
    img_url {Faker::LoremFlickr.image}
    video_url {"https://www.youtube.com/watch?v=XqZsoesa55w"}
    views {Faker::Types.rb_integer }
    likes {Faker::Types.rb_array(len: 2, type: -> { SecureRandom.uuid  })}
    dislikes {Faker::Types.rb_array(len: 2, type: -> { SecureRandom.uuid  })}
    created_by_id {1}
    tags {[]}
  end
end
