FactoryBot.define do
  factory :user, class: User do
    username { Faker::Internet.username }
    full_name { Faker::Name.name }
    email { Faker::Internet.email }
    password_digest { BCrypt::Password.create('password') }
    is_active { true }
    img {Faker::LoremFlickr.image}
    subscribers {Faker::Types.rb_integer }
    subscribed_users {Faker::Types.rb_array(len: 2, type: -> { SecureRandom.uuid  })}
  end
end
