Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  namespace :v1 do
    resources :auth, only: [] do
      collection do
        post :signin
        get :me
      end
    end
    jsonapi_resources :users do
      member do
        put :subscribe
        put :unsubscribe
        put :like
        put :dislike
      end
    end

    jsonapi_resources :videos
  end
end
