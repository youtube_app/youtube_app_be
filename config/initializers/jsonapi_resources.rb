JSONAPI.configure do |config|
  # :underscored_key, :camelized_key, :dasherized_key, or custom
  config.json_key_format = :camelized_key

  # :underscored_route, :camelized_route, :dasherized_route, or custom
  config.route_format = :underscored_route

  # :integer, :uuid, :string, or custom (provide a proc)
  config.resource_key_type = :integer

  config.top_level_meta_include_record_count = true
  config.top_level_meta_record_count_key = :record_count

  config.top_level_meta_include_page_count = true
  config.top_level_meta_page_count_key = :page_count

  config.default_paginator = :paged
  config.default_page_size = 25
  config.maximum_page_size = 500
  config.allow_filter = true

  config.default_processor_klass = JSONAPI::Authorization::AuthorizingProcessor
  config.exception_class_whitelist = [Pundit::NotAuthorizedError, ActiveRecord::RecordInvalid]
  config.warn_on_missing_routes = false
end
