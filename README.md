# Getting Started with Create React App

Youtube clone using React, Rails 7 and Postgresql. This app use Redux Toolkit, JWT Cookies and Docker.

Here you can find available youtube_app's api

- Ruby & rails version

```
  ruby 3.1.0
  rails 7.0.4
```

- System dependencies

```
  postgresql
```

- Configuration

```
  Database configuration
```

## Available Scripts With Docker-Compose

- Build image

```
docker build -t youtube_app .
```

- Run

```
docker-compose up --build
```

- Initialize the database if there is an error

```
docker-compose run web rails db:create db:migrate db:seed
```

## Available Scripts Without Docker-Compose

- Database creation

```
  rails db:create
```

- Database initialization

```
  rails db:migrate
```

- Seed db for development

```
  rails db:seed
```

- How to run the test suite

```
  bundle exec rspec
```

- Services (job queues, cache servers, search engines, etc.)

- Deployment instructions

- ...

## Usage

User needs to log in to the system to use the app

Click the sign in button at the top right corner of the page

By default, there will be 2 users created. Or you can register new

```
  user1:
    username: admin_user
    password: Password1234
  user2:
    username: guest
    password: Password1234
```

There are pages in the menu bar:

```
  Home: videos random
  Explore: videos with views > 50
  Subscription: videos posted by channels user subscribe to
  Library: videos posted by user
```

Search Video at Navbar

Share the video by clicking the share video button in the top right corner

- A popup will appear, paste the url of the YouTube video you want to share, set the title and description then select share

Click to watch any video

- You can like or dislike the video, as well as click subscribe to the channel to receive notifications every time that channel shares a new video

Logout by clicking on the avatar

