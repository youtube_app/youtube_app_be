# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_03_25_063757) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "username", null: false
    t.string "full_name"
    t.string "email", null: false
    t.string "password_digest"
    t.string "img"
    t.bigint "subscribers", default: 0
    t.string "subscribed_users", default: [], array: true
    t.boolean "is_active", default: true
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email"
    t.index ["subscribed_users"], name: "index_users_on_subscribed_users"
    t.index ["username"], name: "index_users_on_username"
  end

  create_table "videos", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", null: false
    t.string "desc"
    t.string "img_url"
    t.string "video_url", null: false
    t.bigint "views", default: 0
    t.string "likes", default: [], array: true
    t.string "dislikes", default: [], array: true
    t.string "tags", default: [], array: true
    t.uuid "created_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_videos_on_deleted_at"
    t.index ["title"], name: "index_videos_on_title"
    t.index ["video_url"], name: "index_videos_on_video_url"
  end

end
