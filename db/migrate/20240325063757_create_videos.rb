class CreateVideos < ActiveRecord::Migration[7.0]
  def change
    create_table :videos, id: :uuid do |t|
      t.string :title, null: false
      t.string :desc
      t.string :img_url
      t.string :video_url, null: false
      t.bigint :views, default: 0
      t.string :likes, array: true, default: []
      t.string :dislikes, array: true, default: []
      t.string :tags, array: true, default: []
      t.uuid :created_by_id

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :videos, :title
    add_index :videos, :video_url
    add_index :videos, :deleted_at
  end
end
