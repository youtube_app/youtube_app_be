class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, id: :uuid do |t|
      t.string :username, null: false
      t.string :full_name
      t.string :email, null: false
      t.string :password_digest
      t.string :img
      t.bigint :subscribers, default: 0
      t.string :subscribed_users, array: true, default: []

      t.boolean :is_active, default: true

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :users, :email
    add_index :users, :username
    add_index :users, :deleted_at
    add_index :users, :subscribed_users
  end
end
