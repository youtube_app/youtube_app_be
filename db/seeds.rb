# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

user_list = [
  {
    username: 'admin_user',
    full_name: 'Admin user',
    email: 'admin@gmail.com',
    password_digest: BCrypt::Password.create('Password1234'),
    is_active: true,
    img: Faker::LoremFlickr.image
  },
  {
    username: 'guest',
    full_name: 'guest user',
    email: 'guest@gmail.com',
    password_digest: BCrypt::Password.create('Password1234'),
    is_active: true,
    img: Faker::LoremFlickr.image
  },
]

users = User.import(user_list)

Video.create( {
  title:  Faker::Music::GratefulDead.song ,
  desc: Faker::Quote.famous_last_words,
  img_url: "http://img.youtube.com/vi/XqZsoesa55w/0.jpg",
  video_url: "https://www.youtube.com/embed/XqZsoesa55w",
  tags: ["cat"],
  views: 10,
  created_by_id: users.ids.first
})

Video.create( {
  title:  Faker::Music::GratefulDead.song ,
  desc: Faker::Quote.famous_last_words,
  img_url: "http://img.youtube.com/vi/IwPHy17Iu6E/0.jpg",
  video_url: "https://www.youtube.com/embed/IwPHy17Iu6E",
  tags: ["cat"],
  views: 100,
  created_by_id:  users.ids.last
})

Video.create( {
  title:  Faker::Music::GratefulDead.song ,
  desc: Faker::Quote.famous_last_words,
  img_url: "http://img.youtube.com/vi/pifBpLAun6U/0.jpg",
  video_url: "https://www.youtube.com/embed/pifBpLAun6U",
  tags: ["cat"],
  views: 500,
  created_by_id:  users.ids.last
})